## 技术方案
* 标记值支持数字
* [方案文档](http://git.in.codoon.com/zhangtao/personal-doc/blob/master/pageMark/pm.md)

## 使用说明
* 前端现有站点都以加入统计，其余新增站点统计的话联系@官俊或者我增加站点码
* 页面身份描述信息cpm_ids，需要在页面数据加载完成之后优先设置全局爆露，不然该部分信息会无纪录
* 对外暴露API
	* lib.cpm.record：调用自定义信息纪录
		* event: 自定义事件名
		* data: 事件记录数据
		* [code]: 可选，事件统计码。默认使用当前页面站点码＋页面码，可以以这个字断取代
	
	```
	lib.cpm.record({
		event: 'custom_event',
		data: {
			selfdata_1: 123,
			selfdata_2: 'asdas',
			selfdata_3: [1,2,3]
		},
		code: '1231asd.as12'
	})
	```

## TODO::申请埋点码:
```
	页面名：
	页面地址：
	负责人：

	返回：1928399801
```