/*
* @fileoverview page mark system
* @dependences base function window.lib.ajax
* TODO::remove lib depends
* @author wooha for codoon
* @since 20160219
**/
(function (win) {
	/******************utils func**********************/
	var parseObj = (function(){
		var parse = /\?(.+)/.test(win.location.href) ? (win.location.href.match(/\?(.+)/) || [])[1] : "";
		/\#\!/.test(parse) && (parse = parse.replace(/\#\!.*/, ''));
		return (function () {
			var obj = {};
			if (!!parse) {
				var m = parse.split("&");
				for (var i in m) {
					var t = m[i].split("=");
					obj[t[0]] = t[1];
				}
			}
			return obj;
		})()
	})(),

	strXSSCheck = function(str){
		return str.replace(/[\>\<\s]/g, "");
	}

	addEvent = function(o, s, fn){
        o.attachEvent ? o.attachEvent('on' + s, fn) : o.addEventListener(s, fn, false);
        return o;
    },
    
    removeHandler = function (o, s, fn) {
        o.removeEventListener ? o.removeEventListener(s, fn, false) : o.detachEvent("on" + s, fn);
        return o;
    },

    getAttr = function (e, n) {
    	return e && e.getAttribute ? e.getAttribute(n) || "" : ""
    },

    getDataFromMeta = function (n, c) {
    	var a = document.getElementsByTagName('meta') || [];
    	for (var i = 0, j = a.length; i < j; ++i) {
    		var k = getAttr(a[i], 'name');
    		if (k == n) return a[i][c]
    	}
    },

    getCookie = function (n) {
		var r = document.cookie.match("\\b" + n + "=([^;]*)\\b");
		return r ? r[1] : undefined;
	},

	setCookie = function (name, value, expires, path, domain) { 
		var curcookie = name + "=" + encodeURI(value) 
		+((expires) ? ";expires=" + expires.toGMTString() : "") 
		+((path) ? ";path=" + path : "") 
		+((domain) ? ";domain=" + domain : "");
		document.cookie = curcookie;
	},

	extendDeep = function(a, b){
    	for (var k in b) {
			if (toString.call(b[k]) === "[object Object]")
				a[k] ? arguments.callee(a[k], b[k]) : a[k] = b[k];
			else
				(a[k] = b[k]);
		}
		return a
    },

    ajax = (function(){
	    /*
	    * response handler
	    * */
	    var stateChange = function(){
	        if(this.readyState == 4){
	            if(this.status == 200 || this.state == 304){
	                if(this.responseText){
	                    try{
	                        if(this.resFormat != "text"){
	                            var str = JSON.parse(this.responseText);
	                        }else{
	                            var str = this.responseText;
	                        }
	                    }catch(e){
	                        console.log(e.message);
	                        failHandler(this, {'status': {"msg": e.message, "state": 1}});
	                    }
	                    this.callback(str);
	                }else{
	                    failHandler(this, {'status': {"msg": "response empty", "state": 1}});
	                }
	            }else{
	                console.log(this.status);
	                var obj = {
	                    status: {
	                        state: 1,
	                        msg: this.statusText
	                    }
	                },
	                res = {};

	                try {
	                    res = JSON.parse(this.response);
	                } catch(e) {}

	                for (var property in res) {
	                	obj[property] = res[property]
	                }

	                failHandler(this, obj, this.statusText);
	            }
	        }
	    },

	    /*
	    * XHR error handler
	    * */
	    failHandler = function (req, obj, info) {
	    	req.failback(req, obj, info);
	    },
	    /*
	    * set credentials
	    * @param  {Object} xhr
	    * @param  {Object} param
	    * @return {object} xhr
	    * */
	    setCredentials = function (xhr, param) {
	        if (typeof(param.withCredentials) != 'undefined') {
	            try{
	                xhr.withCredentials = param.withCredentials;
	            } catch (e) { console.error(e); }
	        }

	        return xhr;
	    };
	    
	    return function(param){
	        var xhr = null;
	        try{
	            xhr = new XMLHttpRequest();
	        }catch(e){
	            try{
	                xhr = new ActiveXObject("MSXML2.XMLHTTP");
	            }catch(e){
	                try{
	                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
	                }catch(e){
	                    xhr = null;
	                }
	            }
	        }
	        if (!xhr) return false;
	        
	        xhr.method = !!param.type ? param.type : 'GET';
	        xhr.url = !!param.url ? param.url : "";
	        xhr.async = !!param.async ? param.async : true;
	        xhr.data = !!param.data ? param.data : "";
	        xhr.resFormat = !!param.format ? param.format : "json";
	        xhr.cType = !!param.contentType ? param.contentType : "";
	        xhr.callback = !!param.success ? param.success : function(){};
	        xhr.failback = !!param.error ? param.error : function(){};
	        xhr.timeOutCB = !!param.timeOutCB ? param.timeOutCB : '';
	        xhr.result = "";
	        xhr.onreadystatechange = stateChange;
	        
	        if(xhr.method.toUpperCase() == "POST"){
	            xhr.open(xhr.method, xhr.url, xhr.async);

	            var data = xhr.data.constructor == Object ? JSON.stringify(xhr.data) : xhr.data;

	            if (!!xhr.cType)
	                xhr.setRequestHeader("Content-Type", xhr.cType);
	            else if (xhr.data.constructor == Object) {
	                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	            }

	            setCredentials(xhr, param).send(data);
	        }else{
	            if(!!xhr.data){
	                var flag = /\?/.test(xhr.url), data = xhr.data;
	                if(data.constructor == Object){
	                    var s = flag ? '' : '?';
	                    for(key in data)
	                        s += key + '=' + data[key] + '&';

	                    xhr.url += s.slice(0, -1);
	                }else{
	                    data = data.toString();
	                    xhr.url += flag ? data : ('?' + data);
	                }
	            }
	            xhr.open(xhr.method, xhr.url, xhr.async);
	            setCredentials(xhr, param).send(null);
	        }
	    }
	})();
    /**************************************************/
    
	/*
	*	Read those three var after page onloaded
	* */

	var PM_KEY			= 'cpm',				//page mark tag key
		PM_TAG			= 'data-' + PM_KEY,		//page mark att key
		PM_ANCHOR		= PM_TAG + '-anchor',	//page link anchor id
		PM_VTID			= PM_KEY + '_vtid',		//visitor trace unique id
		SESSION_TRACE_KEY = 'xmall_cpm_trace',	//trace data key of previous page

		SITE_CODE		= '',	//site code
		PAGE_CODE		= '',	//page code
		PAGE_IDS		= {},	//page identity info
		PAGE_REF		= '',	//page referer
		PAGE_REF_NAME	= '',	//title of referer
		V_TRACE_ID		= '',	//unique visitor trace id
		START_TIME		= '',	//timestamp of user start view a page
		is_PV_UV_send	= false,
		PRE_TRACE_DATA	= sessionStorage.getItem(SESSION_TRACE_KEY);

	/***********Device Info************/
	var Device 		= (function () {
		var agent   = win.navigator.userAgent;
		var ios 	= agent.match(/ipad|iphone|itouch|ipod/i),
			ratio	= win.devicePixelRatio || 1,
			device  = {};

		device.ref_app = /codoon/i.test(agent) ? 'codoon' : (/micromessenger/i.test(agent) ? 'wechat' : '');

		var reg_device	= /android|iphone|ipad|mac|Windows\ Phone|windows/i,
			reg_os		= /(android|iphone\ OS|ipad|Windows\ Phone|Mac\ OS|windows|linux)(\ |\ X\ |;\ cpu\ os\ )([0-9._]+)/i,
			reg_browser	= /(chrome|safari|firefox|opera|MSIE|360|QQBrowser|NokiaBrowser)[\/\s]([0-9.]+)/i;

		!!ios ? (device.is_ios = true) : 
		/android/i.test(agent) ? 
		(device.is_android = true) : '';

		device.device_model = (agent.match(reg_device) || [])[0];
		/windows/i.test(device.device_model) && (device.device_model = 'PC');

		var _os	= agent.match(reg_os) || [];
		_os.length >= 4 && 
		(device.os = _os[1]) && 
		(device.os_version = _os[3]);

		var _browser = agent.match(reg_browser) || [];
		_browser.length >= 3 && 
		(device.browser = _browser[1]) && 
		(device.browser_version = _browser[2]);

		device.brand 		= device.is_ios ? 'Apple' : 
								(device.is_android ? 'Google' : 
									(device.os == 'windows' ? 'Microsoft' : 
										(device.os == 'Nokia' ? 'Nokia' : device.os)));

		device.resolution 	= [win.screen.width * ratio, win.screen.height * ratio].join('*');

		return device;
	})();
	Device.str = JSON.stringify(Device);

	var isInCDApp = Device.ref_app == 'codoon';

	var user_operation_counter = 0;		//count user's click operate

	/*
	*	get page meta const
	* */
	var SITE_CODE	= getDataFromMeta(PM_KEY, 'content') || '',//获取meta标签，name=‘cpm‘,content的值
		PAGE_CODE	= SITE_CODE && getAttr(document.body, PM_TAG);//获取body标签，data-cpm的值

	/*
	*	use site code and page code as default
	*	if there's no site code in meta tags use url instead.
	* */
	var PRICODE = SITE_CODE && PAGE_CODE ? 
		[SITE_CODE, PAGE_CODE] : 
		['cpm_' + encodeURIComponent(win.location.href.replace(/\?.+/g, '').replace(/\#.+/g)) + '_url'];

	/*
	*	Get cpm mark value
	*	@param {documentElement} dom
	*	@return {String}
	* */
	var getPM = function (dom) {
    	return dom && getAttr(dom, PM_TAG);
    };

	/*
	*	Generate mark code for all nodes on this page which has not the mark code
	* */
	//获取模块cpm码
	var markGenerator = (function () {
		/*
		*	loop wrapper nodes for pm code
		*	@param {DocumentElement} dom
		*	@return {Array} preCodeChain
		*	@return {documentElement} bloodParent
		* */
	    function loopCpmChain (dom) {
	    	var chain 				= [],
	    		hasGotMarkParent 	= false,
	    		markParentElement 	= '';

	    	(function (el) {
	    		var parentDom = el.parentElement;
		    	if (parentDom != document.body) {
		    		var el_pm = getPM(parentDom);
		    		if (el_pm) {
		    			if (!hasGotMarkParent) {
		    				hasGotMarkParent = true;
		    				markParentElement = parentDom;
		    			}

		    			chain.unshift(el_pm);
		    		}

		    		arguments.callee.call(null, parentDom);
		    	} else 
		    		return true;
	    	})(dom);

	    	return {
	    		preCodeChain: chain,
	    		bloodParent: markParentElement
	    	}
	    }

	    /*
	    *	get node index number
	    *	@param {documentElement}	dom
	    *	@return {int}	num
	    * */
	    function getInheritIndex (dom) {
	    	var num = 1;
	    	while (dom.previousElementSibling) {
	    		num += 1;
	    		dom = dom.previousElementSibling;
	    	}

	    	return num;
	    }

	    /*
	    *	calculate anchor node inherit prefix
	    *	@param {dom} parentdOM
	    *	@return {array} prefix
	    * */
	    function getInheritPrefix (parentDom) {
	    	var prefix = [];

	    	(function (el) {
	    		prefix.unshift(getInheritIndex(el));

	    		// el.parentElement != body nop
	    		if (!getPM(el.parentElement)) {
	    			arguments.callee.call(null, el.parentElement);
	    		} else return true;
	    	})(parentDom);

	    	return prefix;
	    }

	    /*
	    *	set anchor id for each domelement in arr
	    *	@param {Array} preCodeChain::cpm chain list(点击链接所在的页面码)
	    *	@param {string} anchorExtendPrefix
	    *	@param {domNode} node::domElement
	    *	@param {int} seq
	    *
	    *	TODO::不鼓励使用A标签指定data-cpm规则，可以使用href指定方案替代
	    * */
	    //设置打开新页面的链接的URL传参数（如：商品列表页面——点击商品——商品详情，这时页面码的值为商品列表页面的值）
	    function setMarkAnchorXX (preCodeChain, anchorExtendPrefix, node, seq) {
	    	var inheritCode = PRICODE.concat(anchorExtendPrefix ? preCodeChain.concat(anchorExtendPrefix) : preCodeChain).join('.');

	    	var node_pmc = getPM(node);
	    	var code = preCodeChain.length > 0 ? ('.' + (node_pmc || seq)) : (node_pmc ? '.' + node_pmc : '');

	    	node.setAttribute(PM_ANCHOR, inheritCode + code);//设置PM_ANCHOR属性的值
	    	appendAnchorToLink(node);
	    }

	    /*
	    *	append anchor id to link href
	    *	@param {documentElement} dom
	    * */
	    //给要打开的页面的a标签的URL添加pm_vt和pm_r参数
	    function appendAnchorToLink (dom) {
	    	var href 		= dom.href,
	    		/*
	    		*	TODO::Fuc this mfs off.
	    		* */
	    		prefix 		= '',
	    		protocal	= 'codoon://www.codoon.com/codoon/web_view?url=',
	    		item_detail	= 'codoon://www.codoon.com/mall/goods_detail';
	    	
	    	/*
	    	*	tel,mail...
	    	* */
	    	var domLink = href.match(/([a-zA-Z]*):/);
	    	if (domLink) {
	    		if (domLink.length > 1 && !(/http|codoon/.test(domLink[1]))) {
	    			return false;
	    		}
	    	}

	    	/*
	    	*	APP
	    	* */
	    	if (isInCDApp) {//app中
	    		if (href.indexOf(protocal) != -1) {
	    			prefix = protocal;
	    			href = href.split(prefix)[1];
	    			href = decodeURIComponent(decodeURI(href));
	    		} else if (/^codoon\:\/\//.test(href) && href.indexOf(item_detail) == -1) {
	    			return false;
	    		}
	    	}

	    	if (/pm_r/.test(href))
	    		return false;

	    	var hasHash = href.match(/#\S+/);
			var hash = hasHash ? hasHash[0] : '';
			href = href.replace(hash, '');

	    	if (href.indexOf('?') == -1)
	    		href += '?';
	    	else
	    		href += '&';

	    	href += 'pm_vt=' + V_TRACE_ID + '&pm_r=' + getAttr(dom, PM_ANCHOR);//h5
	    	isInCDApp && (href += '&pm_r_n=' + document.title);//app
	    	href += hash;
	    	
	    	dom.href = isInCDApp && prefix ? prefix + encodeURIComponent(encodeURI(href)) : href;
	    }

	    /*
	    *	calculate cods fo the dom's sibling nodes
	    *	@param {documentElement} dom
	    * */
	    //获取body内模块的埋点
	    function calculateMarkCodes (dom) {
	    	var anchorExtendPrefix = '';			//anchor extend prefix
	    	
	    	var parentDom = dom.parentElement;
	    	var superObj = loopCpmChain(dom);

			if (superObj.preCodeChain.length == 0) {//不存在cpm
				setMarkAnchorXX(superObj.preCodeChain, anchorExtendPrefix, dom, 1);
			} else {//存在cpm
				if (superObj.bloodParent !== parentDom) {
					/*
					*	for the case of anchor skip dom tree level inherit pm code use prefix to differentiate
					* */
					anchorExtendPrefix = getInheritPrefix(parentDom).join('.');
				}

				var childList = parentDom.children, seq = 0;
				for (j = childList.length; seq < j; seq++) {
					if (childList[seq] == dom) {
						++seq;
						break;
					}
				}

				setMarkAnchorXX(superObj.preCodeChain, anchorExtendPrefix, dom, seq);
			}
	    }

	    /*
	    *	generate page mark code of a
	    *	@param {documentElement} dom
	    *	
	    * */
		function generateMark (dom) {
			calculateMarkCodes(dom);
		}

		/*
		*	egnerate unique visitor trace id
		*	@return {string}
		* */
		function getVTID () {
			var vtid = +new Date + '' + parseInt(Math.random() * 100);
			var expires = new Date(+new Date + 86400000);

			setCookie(PM_VTID, vtid, expires, '/', '.codoon.com');
			return vtid
		}

		return {
			generate: generateMark,
			generateVTID: getVTID
		}
	})();

	var dataHandler = (function () {
		var URI = '//xmall.codoon.com/pm_stat/stat_recv';//发送埋点数据的接口

		/*
		*	Request data constructor
		* */
		var dataCtr = {
			webFor: {／/h5
				//pv|uv
				puObj: function () {
					var param = {
						pm_t: 1,
						pm_c: PRICODE.join('.'),//站点码和页面码
						pm_r: PAGE_REF,//url中的pm_r传参（在页面即将打开时就设置了，如：点击商品列表页面的商品时，就设置了data-cpm-anchor的值，即商品列表页面的站点名.页面码）
						pm_vt: V_TRACE_ID,//url中的pm_vt传参
						device: Device.str//设备信息
					};

					extendDeep(param, PAGE_IDS);

					return param;
				},
				//self define
				customObj: function (obj) {
					var extraData = '', eventName = '';

					if (toString.call(obj) == "[object Object]") {
						extraData = JSON.stringify(obj.data || {});
						eventName = obj.event;
					} else if (toString.call(obj) == "[object String]") {
						eventName = obj;
					}

					return extendDeep({
						pm_t: 2,
						pm_c: obj.code || PRICODE.join('.'),
						pm_r: PAGE_REF,
						pm_vt: V_TRACE_ID,
						event: eventName,
						extra: strXSSCheck(extraData),
						device: Device.str
					}, PAGE_IDS);
				},
				//trace data
				traceObj: function () {
					var param = {
						pm_t: 3,
						pm_c: PRICODE.join('.'),
						pm_r: PAGE_REF,
						pm_vt: V_TRACE_ID,
						time: +new Date - START_TIME,
						extra: JSON.stringify({
							user_click_count: user_operation_counter
						}),
						device: Device.str
					};

					extendDeep(param, PAGE_IDS);

					return param;
				}
			},
			nativeFor: {//app
				traceObj: function () {
					return {
						referer: PAGE_REF_NAME,
						name: PAGE_IDS.name || document.title,
						duraing: +new Date - START_TIME,
						extra: {
							pm_c: PRICODE.join('.'),
							pm_r: PAGE_REF,
							pm_vt: V_TRACE_ID
						}
					}
				}, 
				customObj: function (obj) {
					var extraData = {}, eventName = '';

					if (toString.call(obj) == "[object Object]") {
						extraData = obj.data || {};
						eventName = obj.event;
					} else if (toString.call(obj) == "[object String]") {
						eventName = obj;
					}

					return {
						eventName: eventName,
						params: extendDeep(extraData, {
							pm_c: obj.code || PRICODE.join('.'),
							pm_r: PAGE_REF,
							pm_vt: V_TRACE_ID
						})
					}
				}
			}
		};

		/*
		*	Send custom data
		* 	@param {object} obj::info to record
		* */
		var sendCustomData = function (obj) {
			ajax({
				url: URI,
				data: dataCtr.webFor.customObj(obj || ''),
				withCredentials: true,
				format: 'text'
			});

			if (isInCDApp) {
				var data = {
					functionName: 'native_page_mark_custom',
					data: dataCtr.nativeFor.customObj(obj)
				};

				if (Device.is_android) {
					data = JSON.stringify(data);
					win.jsObj && win.jsObj.codoonHandler && win.jsObj.codoonHandler(data);
				} else {
					win.webkit && win.webkit.messageHandlers && win.webkit.messageHandlers.codoonHandler.postMessage(data);
				}
			}
		},

		/*
		*	Send visit trace data
		* */
		sendTraceData = function (preTraceData) {
			sessionStorage.removeItem(SESSION_TRACE_KEY);

			var preObj 	= {};
			try {
				preObj = JSON.parse(preTraceData);
			} catch (e) {}

			var webData = preObj.web;
				appData = preObj.app;

			ajax({
				url: URI,
				data: webData,
				withCredentials: true,
				format: 'text'
			});

			if (isInCDApp) {
				var data = {
					functionName: 'native_page_mark_exit',
					data: appData
				};

				if (Device.is_android) {
					data = JSON.stringify(data);
					win.jsObj && win.jsObj.codoonHandler && win.jsObj.codoonHandler(data);
				} else {
					win.webkit && win.webkit.messageHandlers && win.webkit.messageHandlers.codoonHandler.postMessage(data);
				}
			}
		},

		/*
		*	Send pv/uv ref data
		* */
		sendPVUV = function () {
			is_PV_UV_send = true;

			ajax({
				url: URI,
				data: dataCtr.webFor.puObj(),
				withCredentials: true,
				format: 'text'
			});
		};

		return {
			sendCustomData: sendCustomData,
			sendPVUV: sendPVUV,
			sendTraceData: sendTraceData,
			/*
			*	Cache trace data of current page. Which to be send by next page visit.
			* */
			cacheTraceData: function () {
				var traceData = {
					web: dataCtr.webFor.traceObj(),
					app: dataCtr.nativeFor.traceObj()
				};

				sessionStorage.setItem(SESSION_TRACE_KEY, JSON.stringify(traceData));
			},
		}
	})();

	/*
	*	check if there's tag a in the path of gaven element
	*	@param {documentElement} tar
	* */
	function helloAnchor (tar) {
		while (!/(A|BODY)/i.test(tar.tagName)) {
			tar = tar.parentElement || document.body;
		}
		return tar;
	}

	/*
	*	click event listener
	* */
	function startListener () {
		addEvent(win, 'click', function (e) {
			var tar = helloAnchor(e.target),
				tn = tar.tagName;

			++user_operation_counter;

			tn === 'A' &&
			!getAttr(tar, PM_ANCHOR) && 
			getAttr(tar, 'href') && 
			markGenerator.generate(tar);

		}, false);


		addEvent(win, Device.is_ios ? 'pagehide' : 'unload', function (e) {
			e = e || win.event;

			dataHandler.cacheTraceData();
			!is_PV_UV_send && dataHandler.sendPVUV();
		}, false);
	}

	/*
	*	Initial page mark info
	* */
	function initPageParameters() {
		V_TRACE_ID		= strXSSCheck(parseObj['pm_vt'] || getCookie(PM_VTID) || markGenerator.generateVTID());
		PAGE_REF		= strXSSCheck(parseObj['pm_r'] || '');
		PAGE_REF_NAME	= strXSSCheck(parseObj['pm_r_n'] || '');
		
		var ids	= win[PM_KEY + '_ids'] || {};
		for (var key in ids) {
			var d_type = ids[key].constructor;

			if (d_type == Array || d_type == Object) 
				PAGE_IDS[key] = JSON.stringify(ids[key]);
			else
				PAGE_IDS[key] = strXSSCheck(ids[key]);
		}

		START_TIME	= +new Date;
	}

	/*
	*	page load completed(页面加载完成)
	* */
	function loadCompleted() {
		removeHandler(document, 'DOMContentLoaded', loadCompleted);

		setTimeout(function () {
			initPageParameters();

			dataHandler.sendPVUV();//统计pv/uv

			PRE_TRACE_DATA && dataHandler.sendTraceData(PRE_TRACE_DATA);//统计访问量

			startListener();

		}, 300);
	}

	/*
	*	Initial mark tool
	*	TODO::
	*		test event stuck compatibility
	* */

	if ( document.readyState === "complete" ) {//页面状态
		loadCompleted();
	} else {
		addEvent(document, 'DOMContentLoaded', loadCompleted, false);
	}

	/*
	*	api expose
	* */
	win.lib = win.lib || {};
	win.lib.cpm = {
		/*
		*	record custom data
		* 	@param {object} obj::info to record
		*	{
		*		event: "",
		*		data: {}
		*	}
		* */
		record: dataHandler.sendCustomData,

		/*
		*	Append site code & page code to a given url
		*	@param {string} url::window.open(url)
		*	@return {string} pmUrl
		* */
		appendPMCodeToUrl: function (url) {
			var hasHash = url.match(/#\S+/);
			var hash = hasHash ? hasHash[0] : '';
			url = url.replace(hash, '');

			if (/pm_r/.test(url) || !url)
	    		return false;

	    	if (url.indexOf('?') == -1)
	    		url += '?';
	    	else
	    		url += '&';

	    	url += 'pm_vt=' + V_TRACE_ID + '&pm_r=' + SITE_CODE + '.' + PAGE_CODE;

	    	return url + hash;
		}
	};
})(window)
